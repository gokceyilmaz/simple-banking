package com.eteration.simplebanking.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Table(name = "transactions")
@Entity
public abstract class Transaction {
    private LocalDateTime date;
    private double amount;
    private String type;
    @Id
    private String approvalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account", referencedColumnName = "account_number")
    private Account account;

    public Transaction(double amount) {
        this.approvalCode = UUID.randomUUID().toString();
        this.amount = amount;
        this.date = LocalDateTime.now();
    }

    public Transaction() {

    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }
}
