package com.eteration.simplebanking.model;

import javax.persistence.Table;

@Table(name = "withdrawal_transaction")
public class WithdrawalTransaction extends Transaction{

    public WithdrawalTransaction(double amount) {
        super(amount);
    }
}


