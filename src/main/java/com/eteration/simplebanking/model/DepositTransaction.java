package com.eteration.simplebanking.model;


import javax.persistence.Table;
@Table(name = "deposit_transaction")
public class DepositTransaction extends Transaction {

    public DepositTransaction(double amount) {
        super(amount);
    }

}
