package com.eteration.simplebanking.services;

import com.eteration.simplebanking.model.*;

import javax.transaction.Transactional;

public interface AccountService {
    Account findAccount(String accountNumber);
    Account deposit(Account account,Double amount);
    Account withdraw(Account account,Double amount) throws InsufficientBalanceException;
    @Transactional
    String post(Account account, DepositTransaction depositTransaction);
    @Transactional
    String post(Account account, WithdrawalTransaction withDrawalTransaction) throws InsufficientBalanceException;

    String post(Account account, PhoneBillPaymentTransaction phoneBillPaymentTransaction) throws InsufficientBalanceException;

}
