package com.eteration.simplebanking.services;

import com.eteration.simplebanking.model.*;
import com.eteration.simplebanking.repository.AccountRepository;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository = new AccountRepository() {
        @Override
        public Account findByAccountNumber(String accountNumber) {
            return null;
        }

        @Override
        public List<Account> findAll() {
            return null;
        }

        @Override
        public List<Account> findAll(Sort sort) {
            return null;
        }

        @Override
        public List<Account> findAllById(Iterable<String> strings) {
            return null;
        }

        @Override
        public <S extends Account> List<S> saveAll(Iterable<S> entities) {
            return null;
        }

        @Override
        public void flush() {

        }

        @Override
        public <S extends Account> S saveAndFlush(S entity) {
            return null;
        }

        @Override
        public <S extends Account> List<S> saveAllAndFlush(Iterable<S> entities) {
            return null;
        }

        @Override
        public void deleteAllInBatch(Iterable<Account> entities) {

        }

        @Override
        public void deleteAllByIdInBatch(Iterable<String> strings) {

        }

        @Override
        public void deleteAllInBatch() {

        }

        @Override
        public Account getOne(String s) {
            return null;
        }

        @Override
        public Account getById(String s) {
            return null;
        }

        @Override
        public <S extends Account> List<S> findAll(Example<S> example) {
            return null;
        }

        @Override
        public <S extends Account> List<S> findAll(Example<S> example, Sort sort) {
            return null;
        }

        @Override
        public Page<Account> findAll(Pageable pageable) {
            return null;
        }

        @Override
        public <S extends Account> S save(S entity) {
            return null;
        }

        @Override
        public Optional<Account> findById(String s) {
            return Optional.empty();
        }

        @Override
        public boolean existsById(String s) {
            return false;
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public void deleteById(String s) {

        }

        @Override
        public void delete(Account entity) {

        }

        @Override
        public void deleteAllById(Iterable<? extends String> strings) {

        }

        @Override
        public void deleteAll(Iterable<? extends Account> entities) {

        }

        @Override
        public void deleteAll() {

        }

        @Override
        public <S extends Account> Optional<S> findOne(Example<S> example) {
            return Optional.empty();
        }

        @Override
        public <S extends Account> Page<S> findAll(Example<S> example, Pageable pageable) {
            return null;
        }

        @Override
        public <S extends Account> long count(Example<S> example) {
            return 0;
        }

        @Override
        public <S extends Account> boolean exists(Example<S> example) {
            return false;
        }
    };

    @Transactional
    @Override
    public Account findAccount(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }
    @Transactional
    @Override
    public Account deposit(Account account,Double amount){
       account.setBalance(account.getBalance() + amount);
       accountRepository.save(account);
       return account;
    }
    @Transactional
    @Override
    public Account withdraw(Account account,Double amount) throws InsufficientBalanceException {
        if(account.getBalance() > amount){
            account.setBalance(account.getBalance() - amount);
            accountRepository.save(account);
            return account;
        } else {
            throw new InsufficientBalanceException("Insufficient Balance");
        }
    }
    //@Transactional
    @Override
    public String post(Account account,DepositTransaction depositTransaction){
        if(depositTransaction != null){
            account = deposit(account,depositTransaction.getAmount());
            depositTransaction.setType("DepositTransaction");
            account.getTransactions().add(depositTransaction);
            accountRepository.save(account);
            return depositTransaction.getApprovalCode();
        }
        return "ERROR";
    }
    @Transactional
    @Override
    public String post(Account account,WithdrawalTransaction withDrawalTransaction) throws InsufficientBalanceException {
        if(withDrawalTransaction != null){
            withdraw(account,withDrawalTransaction.getAmount());
            withDrawalTransaction.setType("WithdrawalTransaction");
            account.getTransactions().add(withDrawalTransaction);
            accountRepository.save(account);
            return withDrawalTransaction.getApprovalCode();
           }else {
            throw new InsufficientBalanceException("Insufficient Balance");
        }
    }
    @Transactional
    @Override
    public String post(Account account,PhoneBillPaymentTransaction phoneBillPaymentTransaction) throws InsufficientBalanceException {
        if(phoneBillPaymentTransaction != null){
            if(account.getBalance() > phoneBillPaymentTransaction.getAmount()){
                account.setBalance( account.getBalance() - phoneBillPaymentTransaction.getAmount());
                phoneBillPaymentTransaction.setType("PhoneBillPaymentTransaction");
                phoneBillPaymentTransaction.setApprovalCode(UUID.randomUUID().toString());
                account.getTransactions().add(phoneBillPaymentTransaction);
                accountRepository.save(account);
                return phoneBillPaymentTransaction.getApprovalCode();
            }else {
                throw new InsufficientBalanceException("Insufficient Balance");
            }
        }
        return "ERROR";
    }

}
