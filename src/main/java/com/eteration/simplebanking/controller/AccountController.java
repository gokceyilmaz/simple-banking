package com.eteration.simplebanking.controller;

import com.eteration.simplebanking.model.Account;
import com.eteration.simplebanking.model.DepositTransaction;
import com.eteration.simplebanking.model.WithdrawalTransaction;
import com.eteration.simplebanking.services.AccountServiceImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account/v1")
public class AccountController {

    private final AccountServiceImpl accountServiceImpl;
    @Autowired
    public AccountController(AccountServiceImpl accountServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
    }

    @GetMapping("{accountNumber}")
    public ResponseEntity<Account> getAccount(@PathVariable("accountNumber")String accountNumber) {
        Account account = accountServiceImpl.findAccount(accountNumber);
        if (account == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(account);
        }
    }

    @PostMapping("/credit/{accountNumber}")
    public ResponseEntity<TransactionStatus> credit(@PathVariable("accountNumber") String accountNumber, @RequestBody DepositTransaction request) {
        TransactionStatus transactionStatus = new TransactionStatus();
        // burada hesap numarası ve talep edilen tutar kullanılarak hesaba para yatırma işlemi gerçekleştirilir
        try {
            Account account = accountServiceImpl.findAccount(accountNumber);
            String approvalCode = accountServiceImpl.post(account, request);
            transactionStatus.setApprovalCode(approvalCode);
            transactionStatus.setStatus("OK");
            return new ResponseEntity<>(transactionStatus, HttpStatus.OK);
        } catch (Exception e) {
            transactionStatus.setStatus(e.getMessage());
            return new ResponseEntity<>(transactionStatus, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/debit/{accountNumber}")
    public ResponseEntity<TransactionStatus> debit(@PathVariable("accountNumber") String accountNumber,
                                                    @RequestBody WithdrawalTransaction request) {
        // burada hesap numarası ve talep edilen tutar kullanılarak hesaptan para çekme işlemi gerçekleştirilir
        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setApprovalCode(request.getApprovalCode());
        try {
            Account account = accountServiceImpl.findAccount(accountNumber);
            transactionStatus.setApprovalCode(accountServiceImpl.post(account,request));
            transactionStatus.setStatus("OK");
            return new ResponseEntity<>(transactionStatus,HttpStatus.OK);
        } catch (Exception e) {
            transactionStatus.setStatus(e.getMessage());
            return new ResponseEntity<>(transactionStatus, HttpStatus.BAD_REQUEST);
        }

    }

}